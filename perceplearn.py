""" Usage: python3 perceplearn.py [-h DEVFILE] TRAININGFILE MODELFILE 
	EX: python3 perceplearn.py -h ../util/devFileSpam.txt ../util/trainingFileSpam.txt ../util/modelFileSpam
	EX: python3 perceplearn.py ../util/trainingFileSpam.txt ../util/modelFileSpam
"""
import sys
import operator
import getopt
from random import shuffle
from collections import deque

trainFileName = "no_file"
modelFileName = "no_file"
classWeightVector = {} #{class1:{word:weight}, class2:{word:weight},...}
classWeightAvgVector = {} #{class1:{word:weightAvg}, class2:{word:weightAvg},...}
classWeightCompare = {} #{class1:weight, class2:weight}
trainData = [] #[class_tag:doc, class_tag:doc,....]
className = set([])
totalDoc = 0
isDevFile = False
DEVFILE = "no_file"
totalDevDoc = 0
devData=[] #[class_tag:doc, class_tag:doc,....]
errorRateThreshold = 0.05
finalWeightVector={} #{class1:{word:weight}, class2:{word:weight},...}

def usage():
	print("Usage:{} -h DEVFILE TRAINFILE MODELFILE".format(sys.argv[0]))

def checkDevFileAndCommandArgu():
	global DEVFILE,isDevFile,trainFileName,modelFileName
	try:
		optlist, args = getopt.gnu_getopt(sys.argv[1:], 'h:')
		for opt,arg in optlist:
			if opt in ("-h"):
				isDevFile = True
				DEVFILE = arg
				readDevFile()
		trainFileName = args[0]
		modelFileName = args[1]
	except getopt.GetoptError:
		print("GetoptError")
		usage()
		sys.exit(1)

def readDevFile():
	global DEVFILE, totalDevDoc
	infile = open(DEVFILE,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		devData.append(line.strip())
	totalDevDoc = len(devData)

def readTrainingFile():
	global className,trainFileName, totalDoc
	infile = open(trainFileName,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		trainData.append(line.strip())
		words = line.split(' ',1)
		className.add(words[0])
	totalDoc = len(trainData)
	if len(className)==0:
		print("Error: No class found! Terminate!")
		sys.exit(1)

def initializeDict():
	global className,trainFileName, classWeightVector, classWeightAvgVector,classWeightCompare
	for name in className:
		classWeightVector[name]={}
		classWeightAvgVector[name]={}
		classWeightCompare[name]=0

def resetClassWeightCompare():
	global classWeightCompare
	for name in className:
		classWeightCompare[name]=0

def calErrorRateByDevfile():
	global devData
	errorRate=0.0
	errorPredictDevDoc=0
	correctPredictDevDoc=0
	for doc in devData:
		words = doc.split()
		acturalValue = ""
		#check the wight vector for each class
		resetClassWeightCompare() #reset before compare
		for w in words:
			if w in className:
				acturalValue = w
			#w = w.lower()
			for name in className:
				classWeightCompare[name] += classWeightVector[name].get(w, 0)
			#calculate the class, which has the maximum weight
			precditedClass = calMaxClassFromWeight()
		#check if the precdited class is the same as the actual one. Also, calculate the error rate
		if precditedClass!=acturalValue:
			errorPredictDevDoc+=1
		else:
			correctPredictDevDoc+=1
	errorRate = errorPredictDevDoc/totalDevDoc
	return errorRate

def calFinalWeightVector(counterAvg):
	print("Calculating the final average weight factor...")
	for name in className:
		finalWeightVector[name] = {}
		for key, value in classWeightVector[name].items():
			finalWeightVector[name][key] = value - (classWeightAvgVector[name][key]/counterAvg)

def calWeightVector():
	global classWeightVector, trainFileName,trainData,totalDoc, errorRateThreshold, classWeightAvgVector
	correctPredictDoc = 0
	errorPredictDoc = 0
	wQueue = deque([]) #deque([w1, w2, w3, w4,w5]) , w = {class:{word:weight}}
	wAvgQueue = deque([])	#deque([wAvg1, wAvg2, wAvg3, wAvg4,wAvg5]) , wAvg = {class:{word:weight}}
	errorRateQueue = deque([])
	counter=0
	counterAvg=1
	acturalValue=""
	precditedClass=""
	while True:
		correctPredictDoc = 0
		errorPredictDoc = 0
		shuffle(trainData)
		for doc in trainData:
			words = doc.split()
			acturalValue = ""
			#check the wight vector for each class
			resetClassWeightCompare() #reset before compare
			for w in words:
				if w in className:
					acturalValue = w
					continue
				#w = w.lower()
				for name in className:
					classWeightCompare[name] += classWeightVector[name].get(w, 0)
			if acturalValue=="":
				print("actural Value error! Terminate!")
				sys.exit(1)
			#calculate the class, which has the maximum weight
			precditedClass = calMaxClassFromWeight()
			if precditedClass=="":
				print("precdited Class error! Terminate!")
				sys.exit(1)
			#print("precditedClass="+precditedClass+", acturalValue="+acturalValue)
			#check if the precdited class is the same as the actual one. Also, calculate the error rate
			if precditedClass!=acturalValue:
				#print("Not equal!!!")
				errorPredictDoc+=1
				for w in words:
					if w in className:
						continue
					#w = w.lower()
					classWeightVector[acturalValue][w] = classWeightVector[acturalValue].get(w, 0) + 1
					classWeightVector[precditedClass][w] = classWeightVector[precditedClass].get(w, 0) - 1
					#calculate the average weight vector
					classWeightAvgVector[acturalValue][w] = classWeightAvgVector[acturalValue].get(w,0) + 1*counterAvg* classWeightAvgVector[acturalValue].get(w,0)
					classWeightAvgVector[precditedClass][w] = classWeightAvgVector[precditedClass].get(w,0) - 1*counterAvg*classWeightAvgVector[precditedClass].get(w,0)
			else:
				correctPredictDoc+=1
			counterAvg+=1
		counter+=1
		#check the error rate
		if isDevFile == False:
			thisErrorRate = errorPredictDoc/totalDoc
			print("NoDev Iteration = "+str(counter)+", error_rate = "+str(thisErrorRate))
			if counter == 30:
				calFinalWeightVector(counterAvg)
				break
		elif isDevFile:
			thisErrorRate = calErrorRateByDevfile()
			print("WithDev Iteration = "+str(counter)+", error_rate = "+str(thisErrorRate))
			wQueue.append(classWeightVector)
			wAvgQueue.append(classWeightAvgVector)
			errorRateQueue.append(thisErrorRate)
			if counter<5:
				continue
			if errorRateQueue[0]>errorRateQueue[1] and errorRateQueue[1]>errorRateQueue[2] and errorRateQueue[2]>errorRateQueue[3] and errorRateQueue[3]>errorRateQueue[4]:
				wAvgQueue.popleft()
				errorRateQueue.popleft()
				continue
			ret = max(errorRateQueue) - min(errorRateQueue)
			#print(ret)
			#print(max(errorRateQueue))
			#print(min(errorRateQueue))
			if ret < errorRateThreshold:
				target = min(errorRateQueue)
				index = 0
				for f in errorRateQueue:
					if f == target:
						break
					else:
						index+=1
				#print("index="+str(index))
				classWeightVector = wQueue[index]
				classWeightAvgVector = wAvgQueue[index]
				calFinalWeightVector(counterAvg)
				break#break while
			#hold only totally 5 
			wAvgQueue.popleft()
			errorRateQueue.popleft()

def calMaxClassFromWeight():
	global classWeightCompare
	return max(classWeightCompare.items(), key=operator.itemgetter(1))[0]



#Testing
def Test_checkDevFileAndCommandArgu():
	checkDevFileAndCommandArgu()
	print("trainFileName="+trainFileName)
	print("modelFileName="+modelFileName)
	print("DEVFILE="+DEVFILE)


def Test_readTrainingFile():
	checkDevFileAndCommandArgu()
	readTrainingFile()
	'''for doc in trainData:
		print(doc)'''
	print("Total doc="+str(totalDoc))
	for name in className:
		print(name)
	print("class's number="+str(len(className)))

def Test_resetClassWeightCompare():
	for name in className:
		classWeightCompare[name]=100
	resetClassWeightCompare()
	for name in className:
		print(name+"="+str(classWeightCompare.get(name)))

def Test_DevFile():#command line must have -h DEVFILE
	checkDevFileAndCommandArgu()
	for doc in devData:
		print(doc)

def printModelFile():
	outfile = open(modelFileName, 'w')
	for name in className:
		print("ClassName "+name, file=outfile)
	for name in className:
		for key, value in finalWeightVector[name].items():
			print(name+" "+key+" "+str(value), file=outfile)
def main():
	#Testing
	#Test_checkDevFileAndCommandArgu()
	#Test_readTrainingFile()
	#Test_resetClassWeightCompare()
	#Test_DevFile()

	#step1
	print("Scanning data...")
	checkDevFileAndCommandArgu()
	readTrainingFile()
	print("Finish scanning data...")
	#step2
	initializeDict()
	#step3
	print("Prepare to calculate weights...")
	calWeightVector()
	#step4
	print("Print out the model file...")
	printModelFile()

if __name__ == '__main__':
	main()