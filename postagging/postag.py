'''
Usage: python3 postag.py MODEL
EX: python3 postag.py pos.model
EX: python3 postag.py pos.model <../../util/posTestFile  >../../util/posTestResult
EX: python3 postag.py pos.model <../../pos.test >pos.test.out
'''
import sys
from subprocess import call
from subprocess import PIPE
from subprocess import Popen
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import percepclassify
modelFileName = sys.argv[1]
untagWordList = [] #["previousWord:X1 currentWord:X2 nextWord:x3", "previousWord:X2 currentWord:x3 nextWord:x4", .....]
taggedWordList = []

def makeWordList(sentence):
	global untagWordList
	untagWordList = []
	line = "*BOS* "+sentence+" *EOS*"
	words = line.split()
	for i in range(1,len(words)-1):
		wwCur = words[i]
		wwPre = words[i-1]
		wwNext = words[i+1]
		#every element in untagWordList: previousWord:XX currentWord:XXX nextWord:XXXX
		untagWordList.append("previousWord:"+wwPre.lower()+" currentWord:"+wwCur.lower()+" nextWord:"+wwNext.lower())

def tag3():#improved efficiency! => call percepclassify as a module!
	weightByClasses, classes = percepclassify.getWeightByClassesDictAndClasses(modelFileName)
	while True:
		try:
			sentence = input()
			if not sentence:
				break
			lowerSentence = sentence.lower()
			makeWordList(lowerSentence)
			taggedWordList = []
			for untagWord in untagWordList:	
				outStr = str(percepclassify.doClassify(weightByClasses, classes,untagWord))
				taggedWordList.append(outStr.strip())
			ret= ""
			words = sentence.split()
			for i in range (0, len(words)):
				ret = ret +words[i]+"/"+taggedWordList[i]+" "
			print(ret)

		except EOFError:
			break

def tag2():#improved efficiency!
	while True:
		try:
			sentence = input()
			if not sentence:
				break
			lowerSentence = sentence.lower()
			makeWordList(lowerSentence)
			taggedWordList = []
			for untagWord in untagWordList:
				#print("$$$$$$$: "+untagWord)
				p = Popen(["python3", "../percepclassify.py", modelFileName],stdout=PIPE, stdin=PIPE)
				out= p.communicate(input=untagWord.encode())
				outStr = str(out[0].decode())
				#print("$$$: "+outStr)
				taggedWordList.append(outStr.strip())
			ret= ""
			words = sentence.split()
			for i in range (0, len(words)):
				ret = ret +words[i]+"/"+taggedWordList[i]+" "
			print(ret)

		except EOFError:
			break

def tag():
	while True:
		try:
			sentence = input()
			if not sentence:
				break
			lowerSentence = sentence.lower()
			makeWordList(lowerSentence)
			outfile1 = open(temp1File,'w')
			outfile2 = open(temp2File,'w')
			print("", file=outfile1)
			print("", file=outfile2)
			outfile1.close()
			outfile2.close()
			for untagWord in untagWordList:
				outfile = open(temp1File,'w')
				#print("@@@: "+untagWord)
				print(untagWord, file=outfile)
				outfile.close()
				file1 = open(temp1File,'r+')
				file2 = open(temp2File, 'r+')
				call(["python3", "../percepclassify.py", modelFileName], stdin=file1,stdout=file2)
				file1.close()
				file2.close()

				infile = open(temp2File, 'r')
				lines = infile.readlines()
				infile.close()
				taggedWordList.append(lines[0].strip())
				#print("$$$: "+lines[0])
			ret= ""
			words = sentence.split()
			for i in range (0, len(words)):
				ret = ret +words[i]+"/"+taggedWordList[i]+" "
			print(ret)

		except EOFError:
			break

def Test_makeWordList():
	sentence="You are a pig!"
	makeWordList(sentence)
	for s in untagWordList:
		print(s)

def Test_tag():
	'''Influential/JJ members/NNS of/IN the/DT House/NNP Ways/NNP and/CC Means/NNP Committee/NNP 
	introduced/VBD legislation/NN that/WDT would/MD restrict/VB how/WRB the/DT new/JJ savings-and-loan/NN 
	bailout/NN agency/NN can/MD raise/VB capital/NN ,/, creating/VBG another/DT potential/JJ obstacle/NN 
	to/TO the/DT government/NN 's/POS sale/NN of/IN sick/JJ thrifts/NNS ./.'''
	'''Influential members of the House Ways and Means Committee introduced legislation that would restrict how the new savings-and-loan bailout agency can raise capital , creating another potential obstacle to the government 's sale of sick thrifts .'''
	tag()

def main():
	#Testing
	#Test_makeWordList()
	#Test_tag()

	#step1
	#tag()
	tag3()

if __name__ == '__main__':
	main()