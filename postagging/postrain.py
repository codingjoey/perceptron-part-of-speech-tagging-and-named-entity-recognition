'''
Usage: python3 postrain.py [-h DEVFILE] TRAININGFILE MODELFILE
EX:python3 postrain.py -h ../../pos.dev ../../pos.train pos.model
'''
import sys
import getopt
from subprocess import call
trainFile = "no_file"
modelFile = "no_file"
DEVFILE = "no_file"
isDevFile = False
percepTrainFile = "percepTrainFile"
percepDevFile = "percepDevFile"

def usage():
	print("Usage:{} [-h DEVFILE] TRAINFILE MODELFILE".format(sys.argv[0]))

def checkDevFileAndCommandArgu():
	global DEVFILE,isDevFile,trainFile,modelFile
	try:
		optlist, args = getopt.gnu_getopt(sys.argv[1:], 'h:')
		for opt,arg in optlist:
			if opt in ("-h"):
				isDevFile = True
				DEVFILE = arg
		trainFile = args[0]
		modelFile = args[1]
	except getopt.GetoptError:
		print("GetoptError")
		usage()
		sys.exit(1)

def separateWord(wordToBeSeparated):
	words = wordToBeSeparated.split("/")
	return words #words[0]=word, words[1]=POS

def createPercepTrainingFile():
	global trainFile
	infile = open(trainFile, 'r')
	outfile = open(percepTrainFile, 'w')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		line = "*BOS* "+line+" *EOS*"
		words = line.split()
		for i in range(1,len(words)-1):
			wwCur = separateWord(words[i])#separate one word to word and its parts
			wwPre = separateWord(words[i-1])
			wwNext = separateWord(words[i+1])
			#POS previousWord:XX currentWord:XXX nextWord:XXXX
			print(wwCur[1]+" previousWord:"+wwPre[0].lower()+" currentWord:"+wwCur[0].lower()+" nextWord:"+wwNext[0].lower(), file=outfile)
	outfile.close()

def createPercepDevFile():
	global DEVFILE
	infile = open(DEVFILE, 'r')
	outfile = open(percepDevFile, 'w')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		line = "*BOS* "+line+" *EOS*"
		words = line.split()
		for i in range(1,len(words)-1):
			wwCur = separateWord(words[i])#separate one word to word and its parts
			wwPre = separateWord(words[i-1])
			wwNext = separateWord(words[i+1])
			#POS previousWord:XX currentWord:XXX nextWord:XXXX
			print(wwCur[1]+" previousWord:"+wwPre[0].lower()+" currentWord:"+wwCur[0].lower()+" nextWord:"+wwNext[0].lower(), file=outfile)
	outfile.close()

def createPercepModelFile():
	global modelFile, percepDevFile
	if isDevFile:
		call(["python3", "../perceplearn.py", "-h",percepDevFile, percepTrainFile, modelFile ])
	else:
		call(["python3", "../perceplearn.py", percepTrainFile, modelFile ])
#Testing
def Test_separateWord():
	words = separateWord("you/NN")
	print(words[0])
	print(words[1])

def Test_createPercepTrainingFile():
	checkDevFileAndCommandArgu()
	createPercepTrainingFile()

def main():
	#testing
	#Test_separateWord()
	#Test_createPercepTrainingFile()

	#step0:
	checkDevFileAndCommandArgu()
	#step1:
	createPercepTrainingFile()
	#step2:
	if isDevFile:
		createPercepDevFile()
	#step3:
	createPercepModelFile()

if __name__ == '__main__':
	main()
