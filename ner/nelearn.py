'''
Usage: python3 nelearn.py [-h DEVFILE] TRAININGFILE MODELFILE
EX:python3 nelearn.py -h ../../ner.esp.dev ../../ner.esp.train ner.model
'''
import sys
import getopt
from subprocess import call
trainFile = "no_file"
modelFile = "no_file"
DEVFILE = "no_file"
isDevFile = False
percepTrainFile = "percepTrainFile"
percepDevFile = "percepDevFile"

def usage():
	print("Usage:{} [-h DEVFILE] TRAINFILE MODELFILE".format(sys.argv[0]))

def checkDevFileAndCommandArgu():
	global DEVFILE,isDevFile,trainFile,modelFile
	try:
		optlist, args = getopt.gnu_getopt(sys.argv[1:], 'h:')
		for opt,arg in optlist:
			if opt in ("-h"):
				isDevFile = True
				DEVFILE = arg
		trainFile = args[0]
		modelFile = args[1]
	except getopt.GetoptError:
		print("GetoptError")
		usage()
		sys.exit(1)

def separateWord(wordToBeSeparated):
	words = wordToBeSeparated.split("/")
	return words #words[0]=Word, words[1]=POS, words[2]=NER

def separateWordNER(wordToBeSeparated):
	words = wordToBeSeparated.rsplit("/",1)
	return words #words[0]=Word+POS, words[1]=NER

def stripNER(wordToBeStrip):
	words = wordToBeStrip.rsplit("/",1)
	return words[0] #words[0]=Word+POS, words[1]=NER

def createPercepTrainingFile():
	global trainFile
	infile = open(trainFile, 'r', errors='ignore')
	outfile = open(percepTrainFile, 'w')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		line = "*BOS* "+line+" *EOS*"
		words = line.split()
		for i in range(1,len(words)-1):
			wposnerCUR = separateWordNER(words[i])#separate one word to Word + POS, and NER
			NER = wposnerCUR[1]
			wposCur = stripNER(words[i]) #get rid of NER, leave only word+pos
			wposPre = stripNER(words[i-1]) #get rid of NER, leave only word+pos
			wposNext = stripNER(words[i+1]) #get rid of NER, leave only word+pos
			#NER previousWord:wp/posp currentWord:wc/posc nextWord:wn/posn
			print(NER+" previousWord:"+wposPre+" currentWord:"+wposCur+" nextWord:"+wposNext, file=outfile)
	outfile.close()

def createPercepDevFile():
	global DEVFILE
	infile = open(DEVFILE, 'r', errors='ignore')
	outfile = open(percepDevFile, 'w')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		line = "*BOS* "+line+" *EOS*"
		words = line.split()
		for i in range(1,len(words)-1):
			wposnerCUR = separateWordNER(words[i])#separate one word to Word + POS, and NER
			NER = wposnerCUR[1]
			wposCur = stripNER(words[i]) #get rid of NER, leave only word+pos
			wposPre = stripNER(words[i-1]) #get rid of NER, leave only word+pos
			wposNext = stripNER(words[i+1]) #get rid of NER, leave only word+pos
			#NER previousWord:wp/posp currentWord:wc/posc nextWord:wn/posn
			print(NER+" previousWord:"+wposPre+" currentWord:"+wposCur+" nextWord:"+wposNext, file=outfile)
	outfile.close()

def createPercepModelFile():
	global modelFile, percepDevFile
	if isDevFile:
		call(["python3", "../perceplearn.py", "-h",percepDevFile, percepTrainFile, modelFile ])
	else:
		call(["python3", "../perceplearn.py", percepTrainFile, modelFile ])
#Testing



def main():
	#testing

	#step0:
	checkDevFileAndCommandArgu()
	#step1:
	createPercepTrainingFile()
	#step2:
	if isDevFile:
		createPercepDevFile()
	#step3:
	createPercepModelFile()

if __name__ == '__main__':
	main()
