'''
Usage: python3 netag.py MODEL
EX: python3 netag.py ner.model
EX: python3 netag.py ner.model <../../util/nerTestFile  >../../util/nerTestResult
EX: python3 netag.py ner.model <../../ner.esp.test >ner.esp.test.out
'''
import sys
import codecs
from subprocess import call
from subprocess import PIPE
from subprocess import Popen
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import percepclassify
modelFileName = sys.argv[1]
untagWordList = [] #["previousWord:X1 currentWord:X2 nextWord:x3", "previousWord:X2 currentWord:x3 nextWord:x4", .....]
taggedWordList = []

def makeWordList(sentence):
	global untagWordList
	untagWordList = []
	line = "*BOS* "+sentence+" *EOS*"
	words = line.split()
	for i in range(1,len(words)-1):
		wwCur = words[i]
		wwPre = words[i-1]
		wwNext = words[i+1]
		#every element in untagWordList: previousWord:XX currentWord:XXX nextWord:XXXX
		untagWordList.append("previousWord:"+wwPre+" currentWord:"+wwCur+" nextWord:"+wwNext)

def tag3():#improved efficiency! => call percepclassify as a module!
	sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
	weightByClasses, classes = percepclassify.getWeightByClassesDictAndClasses(modelFileName)
	while True:
		try:
			sentence = input()
			if not sentence:
				break
			makeWordList(sentence)
			taggedWordList = []
			for untagWord in untagWordList:	
				outStr = str(percepclassify.doClassify(weightByClasses, classes,untagWord))
				taggedWordList.append(outStr.strip())
			ret= ""
			words = sentence.split()
			for i in range (0, len(words)):
				ret = ret +words[i]+"/"+taggedWordList[i]+" "
			print(ret)

		except EOFError:
			break

def main():
	tag3()

if __name__ == '__main__':
	main()