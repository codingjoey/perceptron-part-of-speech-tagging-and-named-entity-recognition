''' Usage: python3 percepclassify.py MODELFILE
	EX: python3 percepclassify.py ../util/modelFileSpam <../util/spamTestFile.txt >temp.txt 
'''
import sys
import operator
modelFileName = sys.argv[1]
classes = [] #[class1, class2, class3,...]
weightByClasses = {} #{class1:{word:weight}, class1:{word:weight},...}
classNameIndicator="ClassName"

#*********************Call when module!**********************************************
def getWeightByClassesDictAndClasses(modelFileNameLocal):
	infile = open(modelFileNameLocal,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		words = line.split()
		if words[0]==classNameIndicator:
			classes.append(words[1])
		else:
			className = words[0]
			word = words[1]
			weight = words[2]
			if className in weightByClasses:
				weightByClasses[className][word]=weight
			else:
				weightByClasses[className]={}
				weightByClasses[className][word]=weight
	return weightByClasses, classes

def doClassify(weightByClassesLocal, classesLocal, doc):#call when perceepclassify as a module
	while True:
		try:
			ret=""
			words = doc.strip().split()
			weightCompare = {} #{class1:weight, class2:weight,...}
			for name in classesLocal:
				weightCompare[name]=0.0
			for word in words:
				#word = word.lower()
				for name in classesLocal:
					if word not in weightByClassesLocal[name]:
						continue
					weightCompare[name] += float(weightByClassesLocal[name][word])
			ret = max(weightCompare.items(), key=operator.itemgetter(1))[0]
			return ret
		except EOFError:
			break
#**************************************************************************************

def readModelFile():
	infile = open(modelFileName,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		words = line.split()
		if words[0]==classNameIndicator:
			classes.append(words[1])
		else:
			className = words[0]
			word = words[1]
			weight = words[2]
			if className in weightByClasses:
				weightByClasses[className][word]=weight
			else:
				weightByClasses[className]={}
				weightByClasses[className][word]=weight


def classify():
	while True:
		try:
			doc = input()
			if not doc:
				break
			ret=""
			words = doc.strip().split()
			weightCompare = {} #{class1:weight, class2:weight,...}
			for name in classes:
				weightCompare[name]=0.0
			for word in words:
				#word = word.lower()
				for name in classes:
					if word not in weightByClasses[name]:
						continue
					weightCompare[name] += float(weightByClasses[name][word])
			#if 'currentWord:Influential' in words:
			#	for key, value in weightCompare.items():
			#		print("#####Key="+key+", value="+str(value))
			#print("JJ currentWord:Influential="+str(weightByClasses['JJ']['currentWord:Influential'.lower()]))
			#print("JJ previousWord:*BOS*="+str(weightByClasses['JJ']['previousWord:*BOS*'.lower()]))
			#print("JJ nextWord:members="+str(weightByClasses['JJ']['nextWord:members'.lower()]))
			ret = max(weightCompare.items(), key=operator.itemgetter(1))[0]
			print(ret,flush=True)
		except EOFError:
			break

#Testing
def Test_readModelFile():
	readModelFile()
	for name in classes:
		print(name)
	for name in classes:
		for key, value in weightByClasses[name].items():
			print(name+" "+key+" "+str(value))

def main():
	#Testing
	#Test_readModelFile()

	#step1
	readModelFile()
	classify()

if __name__ == '__main__':
	main()