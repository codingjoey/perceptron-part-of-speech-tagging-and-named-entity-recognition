**Perceptron, part-of-speech tagging and named entity recognition**
===============================================================



    

> This is Homework 2 in USC CSCI 544 Natural Language Processing. In this assignment, I create my own discriminative classifier and apply it to two NLP sequence labeling tasks: part-of-speech tagging and named entity recognition. It includes four parts:


----------




Part1: Averaged Perceptron classifier
-------------------------------------

> Hehe, I write an Averaged Perceptron classifier, including perceplearn.py and percepclassify.py. The perceplearn.py use a heldout development set with the option [-h DEVFILE]. The classification program takes its input from STDIN. Output is written to STDOUT immediately after each corresponding line is entered via STDIN.

**Usage:**

 - Please put training_file first and then model_file. You can
   optionally put "-h DEVFILE" in anywhere.
 - python3 perceplearn.py [-h DEVFILE] TRAININGFILE MODELFILE 

```
$ python3 perceplearn.py -h devFileSpam.txt trainingFileSpam.txt modelFileSpam

$ python3 perceplearn.py trainingFileSpam.txt modelFileSpam
```
 **Usage:**

- percepclassify takes STDIN as input and STDOUT as the output. You can redirect a file as an STDIN and rediect the STDOUT to another file.
- python3 percepclassify.py MODELFILE

```
$ python3 percepclassify.py modelFileSpam <testFileSPAM.txt >spam.out

$ python3 percepclassify.py modelFileSpam <devFileSpam.txt >temp.txt
```


----------



Part2: Part-of-speech tagging
-----------------------------

> In the second part of the assignment, I use my averaged perceptron to perform part-of- speech tagging. Here, I create programs and scripts to train a POS tagging model (postrain.py), and to tag new text (postag.py).

**Usage:** 

 - python3 postrain.py [-h DEVFILE] TRAININGFILE MODELFILE


```
$ python3 postrain.py -h pos.dev pos.train pos.model
```

**Usage:** 

 - python3 postag.py MODEL
```
$ python3 postag.py pos.model

$ python3 postag.py pos.model < posTestFile  > posTestResult

$ python3 postag.py pos.model <../../pos.test >pos.test.out
```



----------



Part3: Named entity recognition
-----------------------------

> In the third part of the assignment you will use your averaged
> perceptron to perform named entity recognition.

 **Usage:** 

 - python3 nelearn.py [-h DEVFILE] TRAININGFILE MODELFILE
 - The output file includes the NER tags produced by my system

```
$ python3 nelearn.py -h ner.esp.dev ner.esp.train ner.model
```

**Usage:** 

 - python3 netag.py MODEL
```
$ python3 netag.py ner.model
$ python3 netag.py ner.model <nerTestFile  >nerTestResult
$ python3 netag.py ner.model <../../ner.esp.test >ner.esp.test.out
```


----------


Part3: Questions answering
-----------------------------


**Question 1:**
>What is the accuracy of your part-of-speech tagger?

Tagged_Filename    | Accuracy
------------------ | ------------------
pos.dev            | 0.9134531495376025

**Learning Process:**

Iteration_times    | ErrorRate
------------------ | -------------------
1                  | 0.09853677991873769
2                  | 0.09165690355709549
3                  | 0.08776827778747165
4                  | 0.08657177755066431
5                  | 0.08318169354637685
6                  | 0.08210982875090361
7                  | 0.07954233865942119
8                  | 0.08417877707704963


**Question 2:**
>What are the precision, recall and F-score for each of the named entity types for your named entity recognizer, and what is the overall F-score?

Entity  | Precision         | Recall            |F-score
--------| ------------------|-------------------|------------
Overall | 0.7715355805243446| 0.5406824146981627|0.6358024691358024
PER     | 0.9102564102564102| 0.4251497005988024|0.5795918367346938
LOC     | 0.75| 0.7105263157894737|0.7297297297297298
ORG     | 0.6875| 0.4489795918367347|0.5432098765432098
MISC    | 0.38461538461538464| 0.38461538461538464|0.38461538461538464


**Question 3:**
>What happens if you use your Naive Bayes classifier instead of your perceptron classifier (report performance metrics)? Why do you think that is?

Classifier   | Data set| Accuracy
------------ | ------- | ------------------
Perceptron   | POS tag | 0.9134531495376025
Naive Bayes  | POS tag | 0.902061470199666

Here I use exactly the same feature I used in perceptron for Naive Bayes classifier, which is "previousWord, currentWord, nextWord". I got almost the same accuracy but much faster learning time with Naive Bayes. If I only use currentWord as a feature for Naive Bayes, the accuracy will be much lower than above, because it doesn't count the sentence environment. So, why perceptron? If the data set is huge. Once the learning process is done. With Naive Bayes, when there are new data, we need to learn again from the whole data set. With perceptron, on the other hand, we can only learn the new data incrementally. This is a big benefit when using perceptron, because the data volume is really huge nowadays and the limited storage will be a problem to store the whole data set. Also, learning from the whole data set every time when there is new data is really time consuming.